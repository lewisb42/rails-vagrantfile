# Vagrantfile for Ruby-on-Rails #

This Vagrantfile sets up a Virtualbox virtual machine configured for development with Ruby-on-Rails.

### Configuration Summary ###

* Linux distribution: Ubuntu 12.04 (Precise Pangolin) 32-bit
* Ruby 2.2.3
* Rails 4.2.5
* rbenv with rbenv-build

### Windows Setup ###

1. Install [Cygwin](http://cygwin.org). It is recommended you install git during the Cygwin setup process.
1. cd to your project directory
1. mkdir devt
1. copy the latest Vagrantfile to your project directory
1. vagrant up

And you're ready to go.  The login credentials are:

username: vagrant
password: vagrant

### Mac Setup ###

You crazy people are on your own.

### Notes ###

Why 32-bit Ubuntu? Because some people using this might be stuck with a 32-bit host. This was the easiest way to avoid compatibility issues.