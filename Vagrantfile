Vagrant.configure("2") do |config|
  
  config.vm.box = "hashicorp/precise32"

  config.vm.provider :virtualbox do |vg|
	vg.gui = true
  end

  
  config.vm.network "private_network", type: "dhcp"
  config.vm.network :forwarded_port, guest: 3000, host: 3000, auto_correct: true

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  config.vm.synced_folder "./work", "/work"

  
  config.vm.post_up_message = <<-POST_UP_MSG
	CS3212 Ruby on Rails Linux Box
	------------------------------
	Methods for accessing the virtual machine:
	1. Directly via VirtualBox (use startx to start the graphical desktop), or
	2. Login over ssh with credentials:
			username: vagrant
			password: vagrant
		or,
	3. (Recommended) Windows users: from a Cygwin (or CMD) terminal you can:
	
			vagrant ssh
		
		to connect to your Ruby on Rails development environment.
	
	Please keep your projects in /work or a subdirectory.  This is mapped to the local filesystem on the host and will not get deleted by vagrant if the virtual machine is destroyed.
  POST_UP_MSG
  
  config.vm.provision "shell", inline: "sudo apt-get update > /dev/null"
 
  # basic packages
  config.vm.provision "shell", inline: "sudo apt-get install -y nano make autoconf sqlite3 > /dev/null"
  
  # rails stuff
  config.vm.provision "shell", inline: "sudo apt-get install -y apache2 curl git nodejs libsqlite3-dev > /dev/null"

  rails_install_script = <<-RAILS_INSTALL_SCRIPT
	echo "Installing rbenv..."
	git clone --quiet https://github.com/sstephenson/rbenv.git ~/.rbenv
	
	# make rbenv usable for this interactive (but non-login) shell
	export PATH="$HOME/.rbenv/bin:$PATH"
	eval "$(rbenv init -)"
	
	# make rbenv usable by login shells
	echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bashrc
	echo 'eval "$(rbenv init -)"' >> ~/.bashrc
	
	echo "Installing rbenv-build..."
	git clone --quiet https://github.com/rbenv/ruby-build.git ~/.rbenv/plugins/ruby-build
	
	echo "Installing Ruby 2.2.3 as the global Ruby"
	rbenv install 2.2.3 > /dev/null
	rbenv global 2.2.3 > /dev/null
	
	echo "Installing Rails 4.2.5"
	gem install rails --version=4.2.5 --quiet
	
	echo "Installing RSpec 3.0 and Cucumber 2.1.0"
	echo 'source "https://rubygems.org"' > ~/Gemfile
	echo "gem 'rspec', '~> 3.0'" >> ~/Gemfile
	echo "gem 'cucumber', '~> 2.1.0'" >> ~/Gemfile
	bundle install
  RAILS_INSTALL_SCRIPT
  
  config.vm.provision "shell", inline: rails_install_script, privileged: false
  
  # uncomment the following line to install graphical desktop (takes a lot more time!)
  config.vm.provision "shell", inline: "sudo apt-get -qq install -y ubuntu-desktop"
end
